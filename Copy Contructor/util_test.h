/*  ===================================================================

*                 T R A D I N G   S Y S T E M   A P I (c)
*    Copyright (c) 1999 - 2015 by Peter Ritter
*    Copyright (c) 2016 by Cygnus Systems Ltd. (www.TradingSystemAPI.com)
*
*    Redistribution and use in source and binary forms, with or without
*    modification, are permitted provided that the following conditions
*    are met:
*
*       1. Redistributions of source code must retain the above copyright
*          notice, this list of conditions and the following disclaimer.
*
*       2. Redistributions in binary form must reproduce the above copyright
*          notice, this list of conditions and the following disclaimer in the
*          documentation and/or other materials provided with the distribution.
*
*       3. The names of its contributors may not be used to endorse or promote
*          products derived from this software without specific prior written
*          permission.
*
*     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*     "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*     LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*     A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
*     CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
*     EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
*     PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
*     PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
*     LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
*     NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
*     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*	 
*  ====================================================================
*/

#ifndef TSA_TEST__INCLUDED
#define TSA_TEST__INCLUDED

#include <string>
#include <exception>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <chrono>

namespace test_framework
{
	struct functions {
		static void fail(const char* msg, const char* file, int line) {
			std::string msg_str = std::string(msg) + "  File: "
				+ std::string(file) + "  Line: " + std::to_string(line);
			throw std::logic_error(msg_str.c_str());
		}

		static bool diff_within_tolerance(double a, double b, double tolerance) {
			if (tolerance < 0.0 || tolerance > 0.001) {
				throw std::logic_error(("given floating-point discrepancy tolerance invalid: "
					+ std::to_string(tolerance)).c_str());
			}
			if (a == b) {
				return true;
			}
			double abs_diff = std::fabs(a - b);
			if (abs_diff < tolerance) {
				return true;
			}
			if (abs_diff > 0.001) {
				return false;
			}
			//for large numbers tolerance is a fractional value
			double adj_tolerance = std::fabs(a * tolerance);
			if (adj_tolerance > 0.001) {
				adj_tolerance = 0.001;
			}
			return abs_diff <= adj_tolerance;
		}
	};

	class test_timer {
		std::chrono::steady_clock::time_point m_begin;
		std::chrono::steady_clock::time_point m_end;
		bool m_is_running = false;
	public:
		enum init_flag {
			// Construct the timer object, but do not start timing. @sa start()
			stopped,
			// Start the timer immediately upon construction.
			started
		};

	public:
		test_timer(init_flag flag = started) {
			if (flag == started) {
				start();
				m_is_running = true;
			}
			else {
				m_is_running = false;
			}
		}

		void start(void) {
			if (m_is_running)
				throw std::runtime_error("cannot start timer; timer is already running!");
			m_begin = std::chrono::steady_clock::now();
			m_is_running = true;
		}

		void reset(void) {
			m_is_running = false;
		}

		const test_timer& stop(void) {
			if (!m_is_running) {
				throw std::runtime_error("timer was not started. cannot stop!");
			}
			m_end = std::chrono::steady_clock::now();
			m_is_running = false;
			return *this;
		}

		double seconds(void) const {
			return milliseconds() / 1000.0;
		}

		double milliseconds(void) const {
			return double(std::chrono::duration_cast<std::chrono::milliseconds>(m_end - m_begin).count());
		}
	};
}

//==============
// dual ostream
//==============

class test_ostream : public std::ofstream {
public:
	std::basic_filebuf<char>* m_file_buf_ptr = nullptr;
public:
	test_ostream(void) {
	}

	bool m_cout_enabled = false;

	void to_cout(bool flag = true) {
		if (flag) {
			m_file_buf_ptr = this->rdbuf();
			this->set_rdbuf(std::cout.rdbuf());
		}
		else {
			if (m_file_buf_ptr)
				this->set_rdbuf(m_file_buf_ptr);
		}
	}
};

//==================
// UNIT TEST MACROS
//==================

// =============== !!!! IMPORTANT !!!! ====================
//  User must #define UNIT_TEST_OUTPUT_PATH as an absolute
//  path to location where test results need to be written.
//  The path should be terminated with a '/' character.
//  e.g. "C:/test/output/"
// ========================================================

#define CREATE_TEST_FILE test_ostream __tsa__test_stream; std::string __tsa__test_id;
#define TEST_FILE __tsa__test_stream
#define TEST_CASE_ID __tsa__test_id

//=================
// UNIT TEST BEGIN
//=================

#define TEST_CASE(test_file_name, test_description) \
    { \
        std::cout << #test_file_name << " - " << #test_description << " " << std::flush; \
		__tsa__test_id = #test_file_name; \
        __tsa__test_stream.open(UNIT_TEST_OUTPUT_PATH #test_file_name ".txt", std::ios::out); \
        try \
        { \
		test_framework::test_timer __test_timer__; \
        __tsa__test_stream << ( "test case begin:" #test_description ) << std::endl << std::endl << std::flush; \
           {

//===============
// UNIT TEST END
//===============

#define END \
            } \
            __tsa__test_stream << std::endl << ( "test case end   " ) << std::endl << std::flush; \
			__test_timer__.stop(); \
			if(__test_timer__.seconds() > 4.0){\
				std::cout << "  [seconds:" << __test_timer__.seconds() << "]" << std::endl << std::flush; \
			}else{ std::cout << std::endl << std::flush;} \
        } \
        catch (const std::exception& e) \
        { \
            std::cout << "test case exception:" << std::endl << std::flush; \
            std::cout << e.what() << std::endl; \
            __tsa__test_stream << "test case exception:" << std::endl << std::flush; \
            __tsa__test_stream << e.what() << std::endl << std::flush; \
        } \
        catch (...) \
        { \
            std::cout << "exception: undefined test case exception." << std::endl << std::flush; \
            __tsa__test_stream << "exception: undefined test case exception." << std::endl << std::flush; \
        } \
        __tsa__test_stream.flush(); __tsa__test_stream.close(); \
		__tsa__test_stream.to_cout(false); \
    }

//=================

//#define a(condition) if (condition); \
//        else test_framework::functions::fail("test case assertion fail: a(" #condition ") ", __FILE__, __LINE__)

#define CHECK(condition) if (condition); \
        else test_framework::functions::fail("test case assertion fail: CHECK(" #condition ") ", __FILE__, __LINE__)

#define CHECK_FALSE(condition) if (!condition); \
        else test_framework::functions::fail("test case assertion fail: CHECK_FALSE(" #condition ") ", __FILE__, __LINE__)

//#define REQUIRES(condition) if (condition); \
//        else test_framework::functions::fail("test case assertion fail: CHECK(" #condition ") ", __FILE__, __LINE__)


#define TMSG(msg) std::cout << #msg << std::endl

#define CHECK_THROWS(expr) \
    {bool e = false;try{ expr ;} \
    catch(...){e = true;} if(e == true) {;} \
		else test_framework::functions::fail("test case assertion fail: CHECK_THROWS(" #expr ")", __FILE__, __LINE__); }

#define CHECK_NO_THROW(expr) \
    {bool e = false;try{expr;} \
    catch(...){ e = true;} if(e == false) {;}/*nothing*/ \
		else test_framework::functions::fail("test case assertion fail: CHECK_NO_THROW(" #expr ")", __FILE__, __LINE__); }

#define CHECK_EQUAL_T12(a , b) if(test_framework::functions::diff_within_tolerance(a, b, 1e-12)){;} \
                else test_framework::functions::fail("test case assertion fail: CHECK_EQUAL_T12(" #a "-" #b")", __FILE__, __LINE__);

#define CHECK_EQUAL_T10(a , b) if(test_framework::functions::diff_within_tolerance(a, b, 1e-10)){;} \
        else test_framework::functions::fail("test case assertion fail: CHECK_EQUAL_T10(" #a "-" #b")", __FILE__, __LINE__);

#define CHECK_EQUAL_T9(a , b) if(test_framework::functions::diff_within_tolerance(a, b, 1e-9)){;} \
        else test_framework::functions::fail("test case assertion fail: CHECK_EQUAL_T9(" #a "-" #b")", __FILE__, __LINE__);

#define CHECK_EQUAL_T8(a , b) if(test_framework::functions::diff_within_tolerance(a, b, 1e-8)){;} \
        else test_framework::functions::fail("test case assertion fail: CHECK_EQUAL_T8(" #a "-" #b")", __FILE__, __LINE__);

#define CHECK_EQUAL_T7(a , b) if(test_framework::functions::diff_within_tolerance(a, b, 1e-7)){;} \
        else test_framework::functions::fail("test case assertion fail: CHECK_EQUAL_T7(" #a "-" #b")", __FILE__, __LINE__);

#define CHECK_EQUAL_T6(a , b) if(test_framework::functions::diff_within_tolerance(a, b, 1e-6)){;} \
        else test_framework::functions::fail("test case assertion fail: CHECK_EQUAL_T6(" #a "-" #b")", __FILE__, __LINE__);

#define CHECK_EQUAL_T5(a , b) if(test_framework::functions::diff_within_tolerance(a, b, 1e-5)){;} \
        else test_framework::functions::fail("test case assertion fail: CHECK_EQUAL_T5(" #a "-" #b")", __FILE__, __LINE__);

#define CHECK_EQUAL_T4(a , b) if(test_framework::functions::diff_within_tolerance(a, b, 1e-4)){;} \
        else test_framework::functions::fail("test case assertion fail: CHECK_EQUAL_T4(" #a "-" #b")", __FILE__, __LINE__);

#define CHECK_EQUAL_T3(a , b) if(test_framework::functions::diff_within_tolerance(a, b, 1e-3)){;} \
        else test_framework::functions::fail("test case assertion fail: CHECK_EQUAL_T3(" #a "-" #b")", __FILE__, __LINE__);

#define CHECK_EQUAL_T2(a , b) if(test_framework::functions::diff_within_tolerance(a, b, 1e-2)){;} \
        else test_framework::functions::fail("test case assertion fail: CHECK_EQUAL_T2(" #a "-" #b")", __FILE__, __LINE__);

//======================
// Memory Leak Detector 
//======================

#ifdef _MSC_VER
#include <crtdbg.h>

//C++ exception specification ignored except to indicate a function is not __declspec(nothrow)
#pragma warning( disable : 4290 )    

struct check_memory {
	_CrtMemState state1;
	_CrtMemState state2;
	_CrtMemState state3;

	check_memory(void) {
		_CrtMemCheckpoint(&state1);
	}
	
	~check_memory(void) throw(std::exception) {
		_CrtMemCheckpoint(&state2);
		if (_CrtMemDifference(&state3, &state1, &state2)) {
			//_CrtMemDumpStatistics(&state3);
			throw std::logic_error("memory leak!");
		}
	}
};
#else
	struct check_memory{
		check_memory(void) {
// needs to be implemented for linux and mac
		}
		~check_memory(void)
		{}
	};
#endif  // memory leak detector

#endif  //include guard
