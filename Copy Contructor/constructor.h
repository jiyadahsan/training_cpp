#pragma once
#include <iostream>
#include <string>

using namespace std;
class MyClass
{
private:
	string *name;
	int age;

public:

	MyClass();
	MyClass(string);
	MyClass(int);
	MyClass(string name, int age);

	int getAge() {
		return age;
	}
	string * getName() {
		return name;
	}

};

MyClass::MyClass()
{
	cout << endl << "Default Constructor..." << endl;
}

MyClass::MyClass(string iname)
{
	name = new string(iname);
	cout << "name: " << *name << endl;
}

MyClass::MyClass(int iage)
{
	this->age = iage;
	cout << "age: " << iage << endl;
}
MyClass::MyClass(string name, int age) {
	this->name = new string(name);
	this->age = age;
}
