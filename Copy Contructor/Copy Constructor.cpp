#include <iostream>
#include "test_training.h"

#include "copy constructor.h"
#include "constructor.h"

using namespace std;


void run_tests_2000(void) {

	CREATE_TEST_FILE


	TEST_CASE(T2000, Constructor) {
		
		MyClass test;
		MyClass test1("sample");
		CHECK(*test1.getName() == "sample");
		MyClass test2(56);
		CHECK(test2.getAge() == 56);
		MyClass test3("test", 23);
		CHECK(*test3.getName() == "test");
		CHECK(test3.getAge() == 23);

	}END

	TEST_CASE(T2010, Copy Constructor) {
		Person p("XYZ", 34);
		Person p_copy = p;
		CHECK(*p.getName() == *p_copy.getName());
		CHECK(p.getAge() == p_copy.getAge());
		TEST_FILE << "name: " << *p.getName() << endl;
		TEST_FILE << "copy_name: " << *p_copy.getName() << endl;
		TEST_FILE << "age: " << p.getAge() << endl;
		TEST_FILE << "copy_age: " << p_copy.getAge() << endl;
		//TEST_FILE.to_cout();

	}END

	TEST_CASE(T2020, Assignment Constructor) {
		Person p("XYZ", 34);
		Person q("PQR", 45);
		p = q;
	
		TEST_FILE << "name: " << *p.getName() << endl;
		TEST_FILE << "name: " << *q.getName() << endl;
	

		//TEST_FILE.to_cout();

	}END
}



