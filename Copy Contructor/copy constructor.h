#pragma once
#include <iostream>
#include <string>
using namespace std;

class Person
{
public:
	Person(string name, int age) {
		this->name = new string(name);
		this->age = age;
	}

	Person(const Person &p) {
		cout << endl << "Copy constructor running " << endl;
		name = new string(*p.name);
		age = p.age;
	}
	void changeNameandAge(string name, int age) {
		(*this->name) = name;
		this->age = age;
	}

	void introduce() {
		cout << "i am " << *name << " and " << age << " years old..." << endl;
	}

	int getAge() {
		return age;
	}

	string * getName() {
		return name;
	}

private:
	string* name;
	int age;
};