// Constructor.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
using namespace std;

class MyClass
{
private:
	string *name;
	int age;

public:
	MyClass();
	MyClass(string);
	MyClass(int);
	MyClass(string, int);

	void display() {
	
		cout <<  name << endl << age << endl;
	}


};

MyClass::MyClass()
{
	cout << "Default Constructor..." << endl;
}

MyClass::MyClass(string iname)
{
	name = new string (iname);
	cout << "name: " << *name << endl;
}

MyClass::MyClass(int iage)
{
	cout << "age: " << iage << endl;
}
MyClass::MyClass(string iname, int iage)
{
	cout << "name: " << iname << endl << "age: " << iage << endl;
}


int main()
{
	MyClass test;
	MyClass test1("sample");
	MyClass test2(56);
	MyClass test3("test", 23);

}
