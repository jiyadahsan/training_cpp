// Basic Exception.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>

using namespace std;

void migtGoWrong() {

		bool error = false;
		bool error1 = false;
		bool error2 = true;

		if (error) {
			throw 8;
		}
		if (error1) {
			throw "Something went wrong.";
		}
		if (error2) {
			throw string("Something else went wrong.");
		}
}

void usesMightGoWrong() {
	migtGoWrong();
}

int main()
{
	try
	{
		usesMightGoWrong();
	}
	catch (int e)
	{
		cout << "Error Code: " << e << endl;
	}
	catch (char const * e) {
		cout << "Error message: " << e << endl;
	}
	catch (string & e) {
		cout << "String Error message: " << e << endl;
	}

	cout << "Still running " << endl;

	//getchar();

	return 0;
}


/*
#include <iostream>
#include <string>
using namespace std;

void mightGoWrong() {

	bool error1 = false;
	bool error2 = true;

	if (error1) {
		throw "Something went wrong.";
	}

	if (error2) {
		throw string("Something else went wrong.");
	}

}

void usesMightGoWrong() {
	mightGoWrong();
}



int main() {

	try {
		usesMightGoWrong();
	}
	catch (int e) {
		cout << "Error code: " << e << endl;
	}
	catch (char const * e) {
		cout << "Error message: " << e << endl;
	}
	catch (string &e) {
		cout << "string error message: " << e << endl;
	}

	cout << "Still running" << endl;

	return 0;
}

*/