/*
Debugging Aid C++ Library 
Copyright (c) 2016 - 2018, Cygnus Systems Ltd.
All rights reserved.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
//cr: 2018 - May - 20
#pragma once

#ifndef UTIL_DEBUG__INCLUDED
#define UTIL_DEBUG__INCLUDED

#include <exception>
#include <stdexcept> // required on linux
#include <vector>
#include <string>
#include <cmath> // required on linux

struct debug_support
{
	static std::string make_msg(const char* fmt, const char* c, const char* f, int ln) {
		const int sz = std::snprintf(nullptr, 0, fmt, c, f, ln);
		std::vector<char> buf(sz + 1);
		std::snprintf(&buf[0], buf.size(), fmt, c, f, ln);
		return std::string(&buf[0]);
	}

	static std::string make_msg(const char* fmt, double a, double b, const char* f, int ln) {
		const int sz = std::snprintf(nullptr, 0, fmt, a, b, f, ln);
		std::vector<char> buf(sz + 1);
		std::snprintf(&buf[0], buf.size(), fmt, a, b, f, ln);
		return std::string(&buf[0]);
	}

	static void raise_assert_error(const char* _condition, const char* _file, int _line) {
		const char* format = "%s [FILE: %s]  [LINE: %i]";
		std::string emsg = make_msg(format, _condition, _file, _line);
		throw std::logic_error(emsg.c_str());
	}

	static void raise_runtime_error(const char* _error_msg, const char* _file, int _line) {
		const char* format = "runtime error: %s  [FILE: %s]  [LINE: %i]";
		std::string emsg = make_msg(format, _error_msg, _file, _line);
		throw std::logic_error(emsg.c_str());
	}

	static bool diff_within_tolerance(double a, double b, double tolerance) {
		if (tolerance < 0.0 || tolerance > 0.001) {
			throw std::logic_error(("given floating-point discrepancy tolerance invalid: "
				+ std::to_string(tolerance)).c_str());
		}
		if (a == b) {
			return true;
		}
		const double abs_diff = std::fabs(a - b);
		if (abs_diff < tolerance) {
			return true;
		}
		if (abs_diff > 0.001) {
			return false;
		}
		//for large numbers tolerance is a fractional value
		double adj_tolerance = std::fabs(a * tolerance);
		if (adj_tolerance > 0.001) {
			adj_tolerance = 0.001;
		}
		return abs_diff <= adj_tolerance;
	}

	static void raise_fp_inequality(
		double a, double b, double tolerance, const char* file, int line) {
		if (!diff_within_tolerance(a, b, tolerance)) {
			const char* format = "floating point discrepancy not within tolerance: %.17f vs %.17f in File: %s Line: %i";
			std::string emsg = make_msg(format, a, b, file, line);
			throw std::logic_error(emsg.c_str());
		}
	}
};

//===============
// Verify macros   (both debug and release)
//===============

#define tsa_verify(condition) if(!(condition)){debug_support::raise_assert_error("verify failure(" #condition ")", __FILE__, __LINE__);}
#define tsa_verify_fp_are_equal_t3(a, b) debug_support::raise_fp_inequality(a, b, 1e-3, __FILE__, __LINE__)
#define tsa_verify_fp_are_equal_t6(a, b) debug_support::raise_fp_inequality(a, b, 1e-6, __FILE__, __LINE__)
#define tsa_verify_fp_are_equal_t9(a, b) debug_support::raise_fp_inequality(a, b, 1e-9, __FILE__, __LINE__)
#define tsa_verify_fp_are_equal_t12(a, b) debug_support::raise_fp_inequality(a, b, 1e-12, __FILE__, __LINE__)

//===============
// Assert macros   (debug only)
//===============
#ifdef TSA_DEBUG
#define tsa_assert(condition) do{ if(!(condition)){debug_support::raise_assert_error("assert failure(" #condition ")", __FILE__, __LINE__);}} while(0)
#define tsa_template_assert(condition)  do{ if(!(condition)){debug_support::raise_assert_error("assert failure(" #condition ")", __FILE__, __LINE__);}} while(0)
#define tsa_assert_fp_are_equal_t3(a, b) debug_support::raise_fp_inequality(a, b, 1e-3, __FILE__, __LINE__)
#define tsa_assert_fp_are_equal_t6(a, b) debug_support::raise_fp_inequality(a, b, 1e-6, __FILE__, __LINE__)
#define tsa_assert_fp_are_equal_t9(a, b) debug_support::raise_fp_inequality(a, b, 1e-9, __FILE__, __LINE__)
#define tsa_assert_fp_are_equal_t12(a, b) debug_support::raise_fp_inequality(a, b, 1e-12, __FILE__, __LINE__)
#else
#define tsa_assert(condition) ((void)0)
#define tsa_template_assert(condition) ((void)0)
#define tsa_assert_fp_are_equal_t3(a, b)  ((void)0)
#define tsa_assert_fp_are_equal_t6(a, b)  ((void)0)
#define tsa_assert_fp_are_equal_t9(a, b)  ((void)0)
#define tsa_assert_fp_are_equal_t12(a, b)  ((void)0)
#endif

#define tsa_throw_not_implemented debug_support::raise_runtime_error("not implemented", __FILE__, __LINE__);
#define tsa_throw_invalid debug_support::raise_runtime_error("invalid control of flow - execution should not get to this point", __FILE__, __LINE__);

#endif
