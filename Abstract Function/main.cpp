#include "Base.h"
#include "Derived.h"
#include "out.h"

int main() {
	
	CREATE_TEST_FILE

		TEST_CASE(T5000, Abstract class) {
		Derived d;
		d.fun();

		CHECK(d.getX() == 5);
		TEST_FILE << "X = " << d.getX() << std::endl;
		//TEST_FILE.to_cout();

	}END



	getchar();
	return 0;
}
