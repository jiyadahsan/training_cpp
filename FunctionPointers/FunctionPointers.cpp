// FunctionPointers.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>

void test() {
	std::cout << "Hello World!\n";
}
void test(double value) {
	std::cout << "Hello -- " << value << std::endl;
}

int main()
{
	test();

	void (*pTest)(double) = test;

	//pTest = &test;

	//(*pTest)();
	
	pTest(8.77);

	return 0;
}
