// Standard Exception.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <exception>

using namespace std;

class CanGoWrong
{
public:
	CanGoWrong();
};

int main()
{
	try
	{
		CanGoWrong wrong;
	}
	catch (bad_alloc &e)
	{
		cout << "Caught Exception: " << e.what() << endl;
	}
	
	cout << "Still running..." << endl;
}

CanGoWrong::CanGoWrong()
{
	char *pMemory = new char[999999];
	delete[] pMemory;
}


/*

class CanGoWrong {
public:
	CanGoWrong() {
		char *pMemory = new char[999999];
		delete[] pMemory;
	}
};

int main() {

	try {
		CanGoWrong wrong;
	}
	catch (bad_alloc &e) {
		cout << "Caught exception: " << e.what() << endl;
	}

	cout << "Still running" << endl;

	return 0;
}

*/
