// Custom Exception.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <exception>

using namespace std;

class MyException : public exception {
public:
	virtual const char* what() const throw() {
		return "Something bad happend!";
	}
};

class Test {
public:
	void GoesWrong() {
		throw MyException();
	}
};

int main() {

	Test test;

	try
	{
		test.GoesWrong();
	}
	catch (MyException &e)
	{
		cout << e.what() << endl;
	}
}